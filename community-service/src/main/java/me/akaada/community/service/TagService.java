package me.akaada.community.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.akaada.community.mapper.ArticleMapper;
import me.akaada.community.mapper.ArticleTagMapper;
import me.akaada.community.mapper.TagMapper;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.ArticleTag;
import me.akaada.community.model.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 22:36
 */
@Service
public class TagService extends ServiceImpl<TagMapper, Tag> {
    @Autowired
    private TagMapper tagMapper;
    @Autowired
    private ArticleTagMapper articleTagMapper;
    @Autowired
    private ArticleMapper articleMapper;

    /**
     * 标签插入：标签不存在则插入，存在则更新（更新当前标签下关联的文章数）
     *
     * @param tagNames 标签名列表
     * @return 插入或更新后的标签信息列表
     * @author Akaada
     * @date 2021/3/21 22:37
     */
    public List<Tag> insertOrUpdateTags(List<String> tagNames) {
        // 插入或更新后的标签信息列表
        List<Tag> tags = new ArrayList<>();

        // 遍历插入或更新
        tagNames.forEach(tagName -> {
            Tag tag = tagMapper.selectOne(new LambdaQueryWrapper<Tag>().eq(Tag::getTagName, tagName));
            // 标签不存在则新建
            if (tag == null) {
                Tag newTag = Tag.builder().tagName(tagName).build();
                tagMapper.insert(newTag);
                tag = newTag;
            } else {
                // 存在则更新关联的文章数
                tag.setRelatedArticleCnt(tag.getRelatedArticleCnt() + 1);
                tagMapper.updateById(tag);
            }

            // 添加标签信息
            tags.add(tag);
        });

        return tags;
    }

    /**
     * 根据标签 id 获取所有关联的文章
     *
     * @param tagId 标签 id
     * @param page 分页参数
     * @return com.baomidou.mybatisplus.extension.plugins.pagination.Page<me.akaada.community.model.entity.Article>
     * @author akaada
     * @date 2021/3/31 21:55
     */
    public Page<Article> selectArticlesByTagId(String tagId, Page<Article> page) {
        List<ArticleTag> articleTags = articleTagMapper.selectList(new LambdaQueryWrapper<ArticleTag>().eq(ArticleTag::getTagId, tagId));
        Set<String> articleIds = Optional.ofNullable(articleTags).orElse(new ArrayList<>()).stream().map(ArticleTag::getArticleId).collect(Collectors.toSet());

        return articleMapper.selectPage(page, new LambdaQueryWrapper<Article>().in(Article::getId, articleIds));
    }
}

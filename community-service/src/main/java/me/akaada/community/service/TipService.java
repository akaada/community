package me.akaada.community.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.akaada.community.mapper.TipMapper;
import me.akaada.community.model.entity.Tip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/24 18:40
 */
@Service
public class TipService extends ServiceImpl<TipMapper, Tip> {
    @Autowired
    private TipMapper tipMapper;

    public Tip getRandomTip() {
        return tipMapper.getRandomTip();
    }
}

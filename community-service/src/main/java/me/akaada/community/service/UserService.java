package me.akaada.community.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.akaada.community.exception.ApiAsserts;
import me.akaada.community.jwt.JwtUtil;
import me.akaada.community.mapper.ArticleMapper;
import me.akaada.community.mapper.FollowMapper;
import me.akaada.community.mapper.UserMapper;
import me.akaada.community.model.dto.LoginDTO;
import me.akaada.community.model.dto.RegisterDTO;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.Follow;
import me.akaada.community.model.entity.User;
import me.akaada.community.model.vo.ProfileVO;
import me.akaada.community.utils.MD5Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 用户服务
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 22:09
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private FollowMapper followMapper;

    /**
     * 根据用户名查找用户信息
     *
     * @param username 用户名
     * @return 用户信息
     * @author Akaada
     * @date 2021/3/21 22:14
     */
    public User getUserByUsername(String username) {
        return userMapper.selectOne(new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    /**
     * 处理用户注册
     *
     * @param dto 注册信息
     * @return 注册后的用户信息
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    public User handleRegister(RegisterDTO dto) {
        User userInDb = getUserByUsername(dto.getName());
        if (userInDb != null) {
            ApiAsserts.fail("账号或邮箱已存在！");
        }

        User newUser = User.builder()
                .username(dto.getName())
                .alias(dto.getName())
                .password(MD5Utils.getPwd(dto.getPass()))
                .email(dto.getEmail())
                .createTime(new Date())
                .modifyTime(new Date())
                .status(true)
                .build();
        userMapper.insert(newUser);

        return newUser;
    }

    /**
     * 处理用户登录
     *
     * @param dto 登录信息
     * @return 登录后的用户信息
     * @author Akaada
     * @date 2021/3/24 11:03
     */
    public String executeLogin(LoginDTO dto) {
        User user = getUserByUsername(dto.getUsername());
        String encodePwd = MD5Utils.getPwd(dto.getPassword());
        if (user == null || !encodePwd.equals(user.getPassword())) {
            ApiAsserts.fail("账号或密码错误");
        }

        // 生成 token
        return JwtUtil.generateToken(user.getUsername());
    }

    public ProfileVO getUserProfile(String userId) {
        ProfileVO profile = new ProfileVO();
        User user = userMapper.selectById(userId);
        BeanUtils.copyProperties(user, profile);

        // 用户文章数
        int count = articleMapper.selectCount(new LambdaQueryWrapper<Article>().eq(Article::getUserId, userId));
        profile.setArticleCnt(count);

        // 粉丝数
        int followers = followMapper.selectCount((new LambdaQueryWrapper<Follow>().eq(Follow::getFolloweeId, userId)));
        profile.setFollowerCnt(followers);

        return profile;
    }
}

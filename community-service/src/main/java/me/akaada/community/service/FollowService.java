package me.akaada.community.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.akaada.community.mapper.BillboardMapper;
import me.akaada.community.mapper.FollowMapper;
import me.akaada.community.model.entity.Billboard;
import me.akaada.community.model.entity.Follow;
import org.springframework.stereotype.Service;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/24 18:40
 */
@Service
public class FollowService extends ServiceImpl<FollowMapper, Follow> {

}

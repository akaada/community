package me.akaada.community.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hankcs.hanlp.seg.common.Term;
import com.hankcs.hanlp.tokenizer.StandardTokenizer;
import com.vdurmont.emoji.EmojiParser;
import me.akaada.community.mapper.ArticleMapper;
import me.akaada.community.mapper.UserMapper;
import me.akaada.community.model.dto.CreateArticleDTO;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.ArticleTag;
import me.akaada.community.model.entity.Tag;
import me.akaada.community.model.entity.User;
import me.akaada.community.model.vo.ArticleVO;
import me.akaada.community.model.vo.ProfileVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 文章服务类
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 19:56
 */
@Service
public class ArticleService extends ServiceImpl<ArticleMapper, Article> {
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TagService tagService;
    @Autowired
    private ArticleTagService articleTagService;
    @Autowired
    private UserService userService;

    /**
     * 获取文章列表
     *
     * @param page 分页参数
     * @param type 文章类型
     * @return 文章列表
     * @author Akaada
     * @date 2021/3/21 20:18
     */
    public Page<ArticleVO> getList(Page<ArticleVO> page, String type) {
        return articleMapper.selectList(page, type);
    }

    /**
     * 文章创建
     *
     * @param dto 用户输入的文章信息
     * @param user 用户信息
     * @return 创建的文章信息
     * @author Akaada
     * @date 2021/3/21 22:18
     */
    @Transactional(rollbackFor = Exception.class)
    public Article create(CreateArticleDTO dto, User user) {
        // 1. 创建文章
        Article article = Article.builder()
                .userId(user.getId())
                .title(dto.getTitle())
                .content(EmojiParser.parseToAliases(dto.getContent()))
                .createTime(new Date())
                .modifyTime(new Date())
                .build();
        articleMapper.insert(article);

        // 2. 更新用户积分
        user.setScore(user.getScore() + 1);
        userMapper.updateById(user);

        // 3. 标签不为空则保存标签列表
        if (!CollectionUtils.isEmpty(dto.getTags())) {
            // 3.1 先插入或更新标签信息
            List<Tag> tags = tagService.insertOrUpdateTags(dto.getTags());

            // 3.2 再插入文章与标签的关联信息
            articleTagService.updateArticleTags(article.getId(), tags);
        }

        return article;
    }

    /**
     * 查看文章详情
     * 
     * @param articleId 文章ID
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @author Akaada
     * @date 2021/3/21 23:22
     */
    public Map<String, Object> selectArticle(String articleId) {
        Map<String, Object> map = new HashMap<>();

        // 1. 根据文章ID查询文章基本信息
        Article article = articleMapper.selectById(articleId);
        Assert.notNull(article, "当前文章不存在，或已被作者删除");
        // 1.1 更新文章浏览数
        article.setViewCnt(article.getViewCnt() + 1);
        articleMapper.updateById(article);
        // 1.2 emoji转码
        article.setContent(EmojiParser.parseToUnicode(article.getContent()));

        // 2. 获取标签信息
        // 2.1 先根据文章ID获取文章关联的所有标签ID
        Set<String> tagIds = articleTagService.list(new LambdaQueryWrapper<ArticleTag>().eq(ArticleTag::getArticleId, articleId)).stream()
                .map(ArticleTag::getTagId).collect(Collectors.toSet());
        // 2.2 再根据标签ID查询对应的标签名
        List<Tag> tags = tagService.listByIds(tagIds);

        // 3. 根据用户ID查询用户信息
        ProfileVO user = userService.getUserProfile(article.getUserId());

        // 4. 查询结果放到 map 中，供前端使用
        map.put("article", article);
        map.put("tags", tags);
        map.put("user", user);

        return map;
    }

    public List<Article> getRecommend(String articleId) {
        return articleMapper.selectRecommend(articleId);
    }

    /**
     * 全文搜索
     *
     * @param keyword 搜索词
     * @param page 分页参数
     * @return 分页后含搜索词的文章列表
     * @author akaada
     * @date 2021/3/21 9:48
     */
    public Page<ArticleVO> searchByKey(String keyword, Page<ArticleVO> page) {
        // 对搜索关键词进行分词、正则化
        String regExpSearchStr = getRegExpSearchStr(keyword);
        // 查询文章
        return articleMapper.searchByKey(page, regExpSearchStr);
    }

    /**
     * 通过分词组件，将搜索词转为 mysql 正则搜索词，配合 regexp 关键词可进行全文搜索
     * 例如，"我是一只小小鸟，怎么飞也飞不高" 通过 HanLP 分词及处理后，转换为 ".*(我|是|一|只|小小鸟|，|怎么|飞|也|飞|不|高|！).*'"
     *
     * @param searchStr 搜索词
     * @return java.lang.String
     * @author akaada
     * @date 2021/3/21 9:45
     */
    private String getRegExpSearchStr(String searchStr) {
        // 1.分词
        List<Term> terms = StandardTokenizer.segment(searchStr);

        // 2. 拼接查询正则语句
        StringBuilder sb = new StringBuilder(".*(");
        for (int i = 0; i < terms.size(); i++) {
            String term = terms.get(i).word;
            // 是否最后一个词原
            if (i != terms.size() - 1) {
                sb.append(term + "|");
            } else {
                sb.append(term);
            }
        }
        sb.append(").*");

        return sb.toString();
    }
}

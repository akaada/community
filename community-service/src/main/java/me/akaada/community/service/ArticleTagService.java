package me.akaada.community.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.akaada.community.mapper.ArticleTagMapper;
import me.akaada.community.model.entity.ArticleTag;
import me.akaada.community.model.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 23:09
 */
@Service
public class ArticleTagService extends ServiceImpl<ArticleTagMapper, ArticleTag> {
    @Autowired
    private ArticleTagMapper articleTagMapper;


    /**
     * 更新文章标签信息：先删除后循环插入
     *
     * @param articleId 文章ID
     * @param tagList 标签列表
     * @author Akaada
     * @date 2021/3/21 23:11
     */
    public void updateArticleTags(String articleId, List<Tag> tagList) {
        // 1. 先删除文章关联的所有的标签
        articleTagMapper.delete(new LambdaQueryWrapper<ArticleTag>().eq(ArticleTag::getArticleId, articleId));

        // 2. 再循环插入文章-标签关联信息
        tagList.forEach(tag -> {
            ArticleTag articleTag = ArticleTag.builder().articleId(articleId).tagId(tag.getId()).build();
            articleTagMapper.insert(articleTag);
        });
    }
}

package me.akaada.community.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.akaada.community.mapper.PromotionMapper;
import me.akaada.community.model.entity.Promotion;
import org.springframework.stereotype.Service;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/24 18:40
 */
@Service
public class PromotionService extends ServiceImpl<PromotionMapper, Promotion> {

}

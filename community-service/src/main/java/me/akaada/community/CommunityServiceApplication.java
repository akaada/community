package me.akaada.community;

import me.akaada.community.jwt.JwtAuthenticationFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/**
 * 服务启动类
 * @SpringBootApplication: SpinrgBoot核心注解，用于开启自动配置
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 15:17
 */
@MapperScan({"me.akaada.community.mapper"})
@EnableEurekaClient
@SpringBootApplication
public class CommunityServiceApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(CommunityServiceApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(CommunityServiceApplication.class);
    }

    @Bean
    public FilterRegistrationBean jwtFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
        registrationBean.setFilter(filter);
        return registrationBean;
    }
}

package me.akaada.community.model.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class Tip implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 内容
     */
    private String content;

    /**
     * 作者
     */
    private String author;

    /**
     * 是否启用 1：是，0：否
     */
    private Boolean isInUse;
}


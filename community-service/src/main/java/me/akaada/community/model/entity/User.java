package me.akaada.community.model.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User implements Serializable {
    private static final long serialVersionUID = -2360549608450683999L;

    /**
     * 用户ID
     */
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户昵称
     */
    private String alias;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 积分
     */
    private Integer score;

    /**
     * token
     */
    private String token;

    /**
     * 个人简介
     */
    private String bio;

    /**
     * 是否激活，1：是，0：否
     */
    private Boolean isActive;

    /**
     * 状态，1：使用，0：停用
     */
    private Boolean status;

    /**
     * 用户角色
     */
    private Integer roleId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;
}


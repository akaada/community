package me.akaada.community.model.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Article implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 作者ID
     */
    private String userId;

    /**
     * 评论统计
     */
    private Integer commentCnt;

    /**
     * 收藏统计
     */
    private Integer collectCnt;

    /**
     * 浏览统计
     */
    private Integer viewCnt;

    /**
     * 是否置顶，1-是，0-否
     */
    private Boolean isTop;

    /**
     * 是否加精，1-是，0-否
     */
    private Boolean isEssence;

    /**
     * 专栏ID
     */
    private Integer sectionId;

    /**
     * 发布时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;


}


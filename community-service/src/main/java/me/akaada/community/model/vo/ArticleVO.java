package me.akaada.community.model.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import me.akaada.community.model.entity.Tag;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 文章VO
 *
 * @author Akaada
 * @date 2021/3/21 20:16
 * @version 0.1
 */
@Data
@NoArgsConstructor
public class ArticleVO implements Serializable {
    private static final long serialVersionUID = -4988190922612573437L;

    /**
     * 文章 id
     */
    private String articleId;

    /**
     * 标题
     */
    private String title;

    /**
     * 作者ID
     */
    private String userId;

    /**
     * 评论统计
     */
    private Integer commentCnt;

    /**
     * 收藏统计
     */
    private Integer collectCnt;

    /**
     * 浏览统计
     */
    private Integer viewCnt;

    /**
     * 是否置顶，1-是，0-否
     */
    private Boolean isTop;

    /**
     * 是否加精，1-是，0-否
     */
    private Boolean isEssence;

    /**
     * 发布时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户昵称
     */
    private String alias;

    /**
     * 账号
     */
    private String username;

    /**
     * 话题关联标签
     */
    private List<Tag> tags;
}


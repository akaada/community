package me.akaada.community.model.entity;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Follow implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 被关注人ID
     */
    private String followeeId;

    /**
     * 关注人ID
     */
    private String followerId;
}


package me.akaada.community.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@TableName("article_tag")
@Accessors(chain = true)
public class ArticleTag implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 标签ID
     */
    @TableField("tag_id")
    private String tagId;

    /**
     * 文章ID
     */
    @TableField("article_id")
    private String articleId;
}


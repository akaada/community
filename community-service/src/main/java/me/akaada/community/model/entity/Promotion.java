package me.akaada.community.model.entity;

import java.io.Serializable;
import lombok.Data;

@Data
public class Promotion implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 广告标题
     */
    private String title;

    /**
     * 广告链接
     */
    private String link;

    /**
     * 说明
     */
    private String description;
}


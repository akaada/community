package me.akaada.community.model.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class CommentDTO implements Serializable {
    private static final long serialVersionUID = -5957433707110390852L;


    /**
     * 文章 id
     */
    private String articleId;

    /**
     * 内容
     */
    private String content;
}

package me.akaada.community.model.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Billboard implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 是否展示中 1：是，0：否
     */
    private Boolean isShow;

    /**
     * 创建时间
     */
    private Date createTime;
}


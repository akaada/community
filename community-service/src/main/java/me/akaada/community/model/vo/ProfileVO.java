package me.akaada.community.model.vo;

import lombok.Data;

@Data
public class ProfileVO {

    /**
     * 用户ID
     */
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 别称
     */
    private String alias;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 关注数
     */
    private Integer followCnt;

    /**
     * 关注者数
     */
    private Integer followerCnt;

    /**
     * 文章数
     */
    private Integer articleCnt;

    /**
     * 专栏数
     */
    private Integer columnCnt;

    /**
     * 评论数
     */
    private Integer commentCnt;
}

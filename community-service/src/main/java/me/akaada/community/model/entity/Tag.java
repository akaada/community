package me.akaada.community.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(chain = true)
@TableName("tag")
@AllArgsConstructor
@NoArgsConstructor
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 标签ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 标签名
     */
    @TableField("tag_name")
    private String tagName;

    /**
     * 关联文章数
     */
    @Builder.Default
    @TableField("related_article_cnt")
    private Integer relatedArticleCnt = 1;
}


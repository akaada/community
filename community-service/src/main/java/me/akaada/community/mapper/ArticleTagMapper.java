package me.akaada.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.akaada.community.model.entity.ArticleTag;
import org.springframework.stereotype.Repository;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 23:08
 */
@Repository
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
}

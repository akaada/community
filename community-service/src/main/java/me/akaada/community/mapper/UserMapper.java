package me.akaada.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.akaada.community.model.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 22:09
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}

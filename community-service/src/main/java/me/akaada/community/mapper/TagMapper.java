package me.akaada.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.akaada.community.model.entity.Tag;
import org.springframework.stereotype.Repository;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 20:31
 */
@Repository
public interface TagMapper extends BaseMapper<Tag> {
}

package me.akaada.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.vo.ArticleVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 19:46
 */
@Repository
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 获取文章列表
     *   - 热门文章筛选规则：最近七天按阅读量和创建时间倒排序
     *   - 其他排序规则：按创建时间到排序
     *
     * @param page 分页参数
     * @param type 文章类型
     * @return 文章列表
     * @author Akaada
     * @date 2021/3/21 20:18
     */
    Page<ArticleVO> selectList(@Param("page") Page<ArticleVO> page, @Param("type") String type);

    /**
     * 获取详情页推荐
     *
     * @author Akaada
     * @return 文章列表
     */
    List<Article> selectRecommend(@Param("articleId") String articleId);

    /**
     * 全文检索
     *
     * @param page 分页参数
     * @param keyword 关键词
     * @return 文章列表
     */
    Page<ArticleVO> searchByKey(@Param("page") Page<ArticleVO> page, @Param("keyword") String keyword);
}

package me.akaada.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.akaada.community.model.entity.Comment;
import org.springframework.stereotype.Repository;

/**
 * @author Akaada
 * @version 0.1
 * @date 2021/3/24 18:40
 */
@Repository
public interface CommentMapper extends BaseMapper<Comment> {

}

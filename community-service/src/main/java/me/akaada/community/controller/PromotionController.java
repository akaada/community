package me.akaada.community.controller;

import me.akaada.community.common.ApiResult;
import me.akaada.community.model.entity.Promotion;
import me.akaada.community.service.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 推广控制器
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/26 9:35
 */
@RestController
@RequestMapping("/promotion")
public class PromotionController {
    @Autowired
    private PromotionService promotionService;

    @GetMapping("/all")
    public ApiResult<List<Promotion>> list() {
        List<Promotion> list = promotionService.list();
        return ApiResult.success(list);
    }
}

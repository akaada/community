package me.akaada.community.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.akaada.community.common.ApiResult;
import me.akaada.community.model.vo.ArticleVO;
import me.akaada.community.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author akaada
 * @version 0.1
 * @date 2021/3/31 23:01
 */
@RestController
@RequestMapping("/search")
public class SearchController {
    @Autowired
    private ArticleService articleService;

    @GetMapping
    public ApiResult<Page<ArticleVO>> searchList(@RequestParam("keyword") String keyword,
                                                 @RequestParam("pageNum") Integer pageNum,
                                                 @RequestParam("pageSize") Integer pageSize) {
        Page<ArticleVO> results = articleService.searchByKey(keyword, new Page<>(pageNum, pageSize));
        return ApiResult.success(results);
    }
}

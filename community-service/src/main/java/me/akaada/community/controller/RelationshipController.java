package me.akaada.community.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import me.akaada.community.common.ApiResult;
import me.akaada.community.model.entity.Follow;
import me.akaada.community.model.entity.Promotion;
import me.akaada.community.model.entity.User;
import me.akaada.community.service.FollowService;
import me.akaada.community.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static me.akaada.community.jwt.JwtUtil.USER_NAME_KEY;

/**
 * 关系控制器
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/26 9:35
 */
@RestController
@RequestMapping("/relationship")
public class RelationshipController {
    @Autowired
    private UserService userService;

    @Autowired
    private FollowService followService;

    /**
     * 处理用户关注
     *
     * @param userName 用户名
     * @param followUserid 关注人用户 id
     * @return 关注结果
     * @author akaada
     * @date 2021/3/27 10:32
     */
    @GetMapping("/follow/{followuserid}")
    public ApiResult<Object> follow(@RequestHeader(value = USER_NAME_KEY) String userName, @PathVariable("followuserid") String followUserid) {
        User user = userService.getUserByUsername(userName);
        if (user == null) {
            return ApiResult.failed("当前用户不存在，无法处理关注操作");
        }

        if (user.getId().equals(followUserid)) {
            return ApiResult.failed("您脸皮太厚了，怎么可以关注自己呢 😮");
        }

        Follow follow = followService.getOne(new LambdaQueryWrapper<Follow>()
                .eq(Follow::getFollowerId, user.getId())
                .eq(Follow::getFolloweeId, followUserid));
        if (follow != null) {
            return ApiResult.failed("已关注，不能重复关注");
        }

        Follow newFollow = Follow.builder().followeeId(followUserid).followerId(user.getId()).build();
        followService.save(newFollow);

        return ApiResult.success(null, "关注成功");
    }

    /**
     * 处理用户取关
     *
     * @param userName 用户名
     * @param unfollowUserid 关注人用户 id
     * @return 取关结果
     * @author akaada
     * @date 2021/3/27 10:32
     */
    @GetMapping("/unfollow/{unfollowuserid}")
    public ApiResult<List<Promotion>> unfollow(@RequestHeader(value = USER_NAME_KEY) String userName, @PathVariable("unfollowuserid") String unfollowUserid) {
        User user = userService.getUserByUsername(userName);
        if (user == null) {
            return ApiResult.failed("当前登录用户非法");
        }

        Follow follow = followService.getOne(new LambdaQueryWrapper<Follow>()
                .eq(Follow::getFollowerId, user.getId())
                .eq(Follow::getFolloweeId, unfollowUserid));
        if (follow != null) {
            return ApiResult.failed("请先关注");
        }

        followService.remove(new LambdaQueryWrapper<Follow>()
                    .eq(Follow::getFollowerId, user.getId())
                    .eq(Follow::getFolloweeId, unfollowUserid));

        return ApiResult.success(null, "取关成功");
    }

    /**
     * 查看用户是否已关注
     *
     * @param userName 用户名
     * @param followUserId 关注用户 id
     * @return 关注结果
     * @author akaada
     * @date 2021/3/27 10:32
     */
    @GetMapping("/isfollow/{followUserId}")
    public ApiResult<Map<String, Object>> isfollow(@RequestHeader(value = USER_NAME_KEY) String userName, @PathVariable("followUserId") String followUserId) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("isFollow", false);

        User user = userService.getUserByUsername(userName);
        if (user == null) {
            return ApiResult.failed("当前登录用户非法");
        }

        Follow follow = followService.getOne(new LambdaQueryWrapper<Follow>()
                .eq(Follow::getFollowerId, user.getId())
                .eq(Follow::getFolloweeId, followUserId));
        if (follow != null) {
            map.put("isFollow", true);
        }

        return ApiResult.success(map);
    }
}

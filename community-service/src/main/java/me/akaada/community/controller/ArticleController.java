package me.akaada.community.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vdurmont.emoji.EmojiParser;
import me.akaada.community.common.ApiResult;
import me.akaada.community.jwt.JwtUtil;
import me.akaada.community.model.dto.CreateArticleDTO;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.ArticleTag;
import me.akaada.community.model.entity.User;
import me.akaada.community.model.vo.ArticleVO;
import me.akaada.community.service.ArticleService;
import me.akaada.community.service.ArticleTagService;
import me.akaada.community.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 文章控制器类
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 19:56
 */
@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private UserService userService;

    @Autowired
    private ArticleTagService articleTagService;

    /**
     * 获取文章列表，包含文章的标签列表
     *
     * @param type 文章类型：latest-最新、hot-最热门
     * @param pageNo 页码，默认 1
     * @param pageSize 每页条数，默认 10
     * @return 文章列表
     * @author Akaada
     * @date 2021/3/14 23:16
     */
    @GetMapping("/list")
    public ApiResult<Page<ArticleVO>> list(@RequestParam(value = "type", defaultValue = "latest") String type,
                                           @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<ArticleVO> list = articleService.getList(new Page<>(pageNo, pageSize), type);
        return ApiResult.success(list);
    }

    /**
     * 创建文章
     *
     * @param userName 用户名
     * @param dto 文章创建 dto
     * @return 创建的文章详情
     * @author Akaada
     * @date 2021/3/18 23:15
     */
    @PostMapping("/create")
    public ApiResult<Article> create(@RequestHeader(value = JwtUtil.USER_NAME_KEY) String userName
            , @RequestBody CreateArticleDTO dto) {
        User user = userService.getUserByUsername(userName);
        Article article = articleService.create(dto, user);
        return ApiResult.success(article);
    }

    /**
     * 查看文章
     *
     * @param articleId 文章 id
     * @return 文章
     * @author Akaada
     * @date 2021/3/18 23:52
     */
    @GetMapping
    public ApiResult<Map<String, Object>> view(@RequestParam("articleId") String articleId) {
        Map<String, Object> map = articleService.selectArticle(articleId);
        return ApiResult.success(map);
    }

    /**
     * 获取推荐文章：非当期文章的随机 10 篇文章
     *
     * @param articleId 当期文章id
     * @return 推荐文章列表
     * @author akaada
     * @date 2021/3/18 23:19
     */
    @GetMapping("/recommend")
    public ApiResult<List<Article>> getRecommend(@RequestParam("articleId") String articleId) {
        List<Article> articles = articleService.getRecommend(articleId);
        return ApiResult.success(articles);
    }

    /**
     * 文章更新
     *
     * @param userName 用户名
     * @param article 文章
     * @return com.douyuehan.doubao.common.api.ApiResult<com.douyuehan.doubao.model.entity.Article>
     * @author akaada
     * @date 2021/3/18 23:34
     */
    @PostMapping("/update")
    public ApiResult<Article> update(@RequestHeader(value = JwtUtil.USER_NAME_KEY) String userName, @Valid @RequestBody Article article) {
        User umsUser = userService.getUserByUsername(userName);
        Assert.isTrue(umsUser.getId().equals(article.getUserId()), "非本人无权修改");
        article.setContent(EmojiParser.parseToAliases(article.getContent()));
        articleService.updateById(article);
        return ApiResult.success(article);
    }

    /**
     * 删除文章
     *
     * @param userName 用户名
     * @param articleId 文章 id
     * @return 删除结果
     * @author akaada
     * @date 2021/3/18 23:51
     */
    @DeleteMapping("/delete/{articleId}")
    public ApiResult<String> delete(@RequestHeader(value = JwtUtil.USER_NAME_KEY) String userName, @PathVariable("articleId") String articleId) {
        // 1. 文章删除校验
        // 1.1 判断文章是否存在
        Article article = articleService.getById(articleId);
        Assert.notNull(article, "来晚一步，文章已不存在");
        // 1.2 判断是否有删除权限（TODO：不只是原作者有删除权限）
        User umsUser = userService.getUserByUsername(userName);
        Assert.isTrue(article.getUserId().equals(umsUser.getId()), "你为什么可以删除别人的文章？？？");

        // 2. 根据文章 id 删除文章信息
        articleService.removeById(articleId);

        // 3. 根据文章 id 删除文章-标签关联信息
        articleTagService.remove(new LambdaQueryWrapper<ArticleTag>().eq(ArticleTag::getArticleId, articleId));

        return ApiResult.success(null,"删除成功");
    }
}

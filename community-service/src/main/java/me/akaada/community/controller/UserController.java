package me.akaada.community.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.akaada.community.common.ApiResult;
import me.akaada.community.model.dto.LoginDTO;
import me.akaada.community.model.dto.RegisterDTO;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.User;
import me.akaada.community.service.ArticleService;
import me.akaada.community.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static me.akaada.community.jwt.JwtUtil.USER_NAME_KEY;

/**
 * 用户相关控制器
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/24 10:45
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    
    @Autowired
    private ArticleService articleService;

    /**
     * 用户注册
     *
     * @param dto 注册信息
     * @return 注册后的用户信息
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    @PostMapping("/register")
    public ApiResult<Map<String, Object>> register(@Valid @RequestBody RegisterDTO dto) {
        User user = userService.handleRegister(dto);
        if (ObjectUtils.isEmpty(user)) {
            return ApiResult.failed("账号注册失败！");
        }

        Map<String, Object> map =  new HashMap<>();
        map.put("user", user);
        return ApiResult.success(map);
    }

    /**
     * 用户登录
     *
     * @param dto 登录信息
     * @return 登录后的 token
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    @PostMapping("/login")
    public ApiResult<Map<String, String>> login(@Valid @RequestBody LoginDTO dto) {
        String token = userService.executeLogin(dto);
        if (ObjectUtils.isEmpty(token)) {
            return ApiResult.failed("账号或密码错误");
        }
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        return ApiResult.success(map, "登录成功");
    }

    /**
     * 用户注销
     *
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    @GetMapping("/logout")
    public ApiResult<Object> logOut() {
        return ApiResult.success(null, "注销成功");
    }

    /**
     * 用户信息获取
     *
     * @param userName 用户名
     * @return 用户信息
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    @GetMapping("/info")
    public ApiResult<User> getUser(@RequestHeader(value = USER_NAME_KEY) String userName) {
        User user = userService.getUserByUsername(userName);
        return ApiResult.success(user);
    }

    /**
     * 用户信息更新
     *
     * @param user 新的用户信息
     * @return 更新后的用户信息
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    @PostMapping("/update")
    public ApiResult<User> updateUser(@RequestBody User user) {
        userService.updateById(user);
        return ApiResult.success(user);
    }

    /**
     * 根据用户名，获取用户信息、用户名下的文章信息
     *
     * @param username 用户名
     * @param pageNo 页码，默认 1
     * @param pageSize 每页条数，默认 10
     * @return 更新后的用户信息
     * @author Akaada
     * @date 2021/3/24 10:47
     */
    @GetMapping("/{username}")
    public ApiResult<Map<String, Object>> getUserByName(@PathVariable("username") String username,
                                                        @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Map<String, Object> map = new HashMap<>();

        // 查询用户信息
        User user = userService.getUserByUsername(username);
        Assert.notNull(user, "用户不存在");

        // 查询用户名下的文章信息
        Page<Article> articles = articleService.page(new Page<>(pageNo, pageSize),
                new LambdaQueryWrapper<Article>().eq(Article::getUserId, user.getId()));

        map.put("user", user);
        map.put("articles", articles);
        return ApiResult.success(map);
    }
}

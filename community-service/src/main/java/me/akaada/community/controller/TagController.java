package me.akaada.community.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.akaada.community.common.ApiResult;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.Tag;
import me.akaada.community.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 标签控制器
 *
 * @author akaada
 * @version 0.1
 * @date 2021/3/28 22:46
 */
@RestController
@RequestMapping("/tag")
public class TagController {
    @Autowired
    private TagService tagService;

    /**
     * 根据标签名获取标签名下的文章、以及其他热门标签
     *
     * @param tagName 标签名
     * @param pageNo 当前页数
     * @param pageSize 每页条数
     * @return 查询结果
     * @author akaada
     * @date 2021/3/31 21:52
     */
    @GetMapping("/{tagname}")
    public ApiResult<Map<String, Object>> listRelatedByTagName(
            @PathVariable("tagname") String tagName,
            @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Map<String, Object> map = new HashMap<>();

        // 根据标签名获取标签详情
        Tag tag = tagService.getOne(new LambdaQueryWrapper<Tag>().eq(Tag::getTagName, tagName));
        Objects.requireNonNull(tag, "话题不存在，或已被管理员删除");

        // 根据标签 id 获取所有关联的文章
        Page<Article> articles = tagService.selectArticlesByTagId(tag.getId(), new Page<>(pageNo, pageSize));

        // 获取其他热门标签
        Page<Tag> hotTags = tagService.page(new Page<>(1, 10), new LambdaQueryWrapper<Tag>()
                .notIn(Tag::getTagName, tagName)
                .orderByDesc(Tag::getRelatedArticleCnt));

        map.put("articles", articles);
        map.put("hotTags", hotTags);

        return ApiResult.success(map);
    }
}

package me.akaada.community.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import me.akaada.community.common.ApiResult;
import me.akaada.community.jwt.JwtUtil;
import me.akaada.community.model.dto.CommentDTO;
import me.akaada.community.model.entity.Article;
import me.akaada.community.model.entity.Comment;
import me.akaada.community.model.entity.User;
import me.akaada.community.service.ArticleService;
import me.akaada.community.service.CommentService;
import me.akaada.community.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


/**
 * 评论控制器
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/26 9:27
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;

    @GetMapping("/list")
    public ApiResult<List<Comment>> listComments(@RequestParam String articleId){
        List<Comment> comments = commentService.list(new LambdaQueryWrapper<Comment>()
                .eq(Comment::getArticleId, articleId));
        return ApiResult.success(comments);
    }

    @PostMapping("/add")
    public ApiResult<Comment> addComment(@RequestHeader(value = JwtUtil.USER_NAME_KEY) String userName,
                                             @RequestBody CommentDTO commentDTO) {
        String articleId = commentDTO.getArticleId();

        // 1. 保存评论信息
        // 1.1 根据用户名查询用户id
        User user = userService.getUserByUsername(userName);
        // 1.2 保存评论信息
        Comment comment = Comment.builder()
                .userId(user.getId())
                .content(commentDTO.getContent())
                .articleId(articleId)
                .createTime(new Date())
                .modifyTime(new Date())
                .build();
        commentService.save(comment);

        // 2. 更新文章的评论数
        Article article = articleService.getById(articleId);
        article.setCommentCnt(article.getCommentCnt() + 1);
        articleService.updateById(article);

        return ApiResult.success(comment);
    }
}

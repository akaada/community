package me.akaada.community.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import me.akaada.community.common.ApiResult;
import me.akaada.community.model.entity.Billboard;
import me.akaada.community.service.BillboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 公告牌控制器
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/26 9:27
 */
@RestController
@RequestMapping("/billboard")
public class BillboardController {
    @Autowired
    private BillboardService billboardService;

    @GetMapping("/show")
    public ApiResult<Billboard> getNotices(){
        List<Billboard> list = billboardService.list(new
                LambdaQueryWrapper<Billboard>().eq(Billboard::getIsShow,true));
        return ApiResult.success(list.get(list.size()- 1));
    }
}

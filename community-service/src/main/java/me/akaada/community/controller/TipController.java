package me.akaada.community.controller;

import me.akaada.community.common.ApiResult;
import me.akaada.community.model.entity.Tip;
import me.akaada.community.service.TipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 控制器
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/26 9:27
 */
@RestController
@RequestMapping("/tip")
public class TipController {
    @Autowired
    private TipService tipService;

    @GetMapping("/today")
    public ApiResult<Tip> getNotices(){
        return ApiResult.success(tipService.getRandomTip());
    }
}

package me.akaada.community.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

public class JwtUtil {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);
    // token 过期时间：5 min
    public static final long EXPIRATION_TIME = 3600_000_000L;
    // 加盐
    public static final String SECRET = "ThisIsASecret";

    public static final String TOKEN_PREFIX = "Bearer ";
    //
    public static final String HEADER_STRING = "Authorization";
    //
    public static final String USER_NAME_KEY = "userName";

    /**
     * 根据用户名生成 token 串
     *
     * @param userName 用户名
     * @return token串
     * @author Akaada
     * @date 2021/3/25 17:40
     */
    public static String generateToken(String userName) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(USER_NAME_KEY, userName);

        String jwt = Jwts.builder()
                .setClaims(map)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        logger.info("用户名：【{}】，生成的 jwt 为；【{}】", userName, jwt);

        return jwt; //jwt前面一般都会加Bearer
    }

    /**
     * 验证 token，同时获取 token 中的 username，添加到 http 请求中
     *
     * @param request 用户请求
     * @return 添加 username 到请求头后的用户请求
     * @author Akaada
     * @date 2021/3/25 17:41
     */
    public static HttpServletRequest validateTokenAndAddUserNameToHeader(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            try {
                Map<String, Object> body = Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody();
                return new CustomHttpServletRequest(request, body);
            } catch (Exception e) {
                logger.info(e.getMessage());
                throw new TokenValidationException(e.getMessage());
            }
        } else {
            throw new TokenValidationException("Missing token");
        }
    }

    public static class CustomHttpServletRequest extends HttpServletRequestWrapper {
        private Map<String, String> claims;

        public CustomHttpServletRequest(HttpServletRequest request, Map<String, ?> claims) {
            super(request);
            this.claims = new HashMap<>();
            claims.forEach((k, v) -> this.claims.put(k, String.valueOf(v)));
        }

        @Override
        public Enumeration<String> getHeaders(String name) {
            if (claims != null && claims.containsKey(name)) {
                return Collections.enumeration(Arrays.asList(claims.get(name)));
            }
            return super.getHeaders(name);
        }

    }

    static class TokenValidationException extends RuntimeException {
        public TokenValidationException(String msg) {
            super(msg);
        }
    }
}
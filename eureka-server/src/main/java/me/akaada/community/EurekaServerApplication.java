package me.akaada.community;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 服务启动类
 * @SpringBootApplication: SpinrgBoot核心注解，用于开启自动配置
 *
 * @author Akaada
 * @version 0.1
 * @date 2021/3/21 14:17
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
}
